﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceCreamBall : MonoBehaviour
{
    public bool ballIsFalling, glueOn, isDown,hasFallen;
    [SerializeField] private float speed;

    public float maxtimerMeltingTic;
    public float timerMeltingTic;

    [SerializeField] private Collider2D bottomCollider;
    [SerializeField] private LayerMask mask;
    //Physics
    private Rigidbody2D rgbd;
    // Start is called before the first frame update
    void Start()
    {
        rgbd = GetComponent<Rigidbody2D>();

        if (maxtimerMeltingTic != 0)
        {
            timerMeltingTic = maxtimerMeltingTic;
        }
        
        ballIsFalling = false;
    }

    // Update is called once per frame
    void Update()
    {

            /*
                if (bottomCollider.IsTouchingLayers(mask))
                {
                    Debug.Log("TouchLayer");
                    isDown = true;
                }
                else
                {
                    Debug.Log("WillFall");
                    isDown = false;
                }
            */
             

            if (!isDown)
            {
                if (ballIsFalling)
                {
                    speed += speed * Time.deltaTime;
                    transform.Translate(Vector2.down*speed*Time.deltaTime);
                }
            }
       
    }

    public void BallIsFalling(bool value)
    {
        ballIsFalling = value;
    }

    public void SetupBall(IceCream ball)
    {
        speed = ball.speedFall;
        GetComponent<SpriteRenderer>().color = ball.spriteColor;
        
        BallMelt ballMelt = GetComponent<BallMelt>();
        ballMelt.scaleReductor = ball.scaleReductor;
        ballMelt.colliderReductor = ball.colliderReductor;
        ballMelt.timerBeforeStartMelting = ball.timeBeforeMelt;
        ballMelt.timerColliderActualisation = ball.timerColliderActualisation;
        ballMelt.saveTimer = ball.timerColliderActualisation;

        
        switch (ball.scaleBall)
        {
            case ScaleBall.Little :
                transform.localScale = Vector3.one * 0.75f; 
                break;
            case ScaleBall.Medium :
                transform.localScale = Vector3.one * 1f; 
                break;
            case ScaleBall.Big : 
                transform.localScale = Vector3.one * 1.25f; 
                break;
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        
    }


    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("IceBall") && other.gameObject.GetComponent<IceCreamBall>().isDown)
        {
            isDown = true;
            rgbd.bodyType = RigidbodyType2D.Dynamic;
            ballIsFalling = false;
            rgbd.gravityScale = 0.25f;
            GetComponent<BallMelt>().meltOn = true;
        }
        else if (other.gameObject.CompareTag("IceCreamCone"))
        {
            isDown = true;
            rgbd.bodyType = RigidbodyType2D.Dynamic;
            ballIsFalling = false;
            rgbd.gravityScale = 0.85f;
            GetComponent<BallMelt>().meltOn = true;
        }

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("FaillingBorder") && !hasFallen)
        {
            hasFallen = true;
            Debug.Log("Invisible");
            GameplayManager.Instance.BallHasFallen();
            GameplayManager.Instance.RemoveBall(this);
        }
        
        
        if (other.gameObject.CompareTag("Border"))
        {
            if (transform.position.x < 0)
            {
                GameplayManager.Instance.dir = Vector2.right;
            }
            else
            {
                GameplayManager.Instance.dir = Vector2.left;
            }
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("IceCreamCone") || other.gameObject.CompareTag("IceBall") && other.gameObject.GetComponent<IceCreamBall>().isDown)
        {
            //isDown = false;
        }
    }
        
}
