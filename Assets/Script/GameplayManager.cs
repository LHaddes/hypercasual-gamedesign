﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

[SerializeField]
public class GameplayManager : MonoBehaviour
{
    public float screenRatio;
    public static GameplayManager Instance;
    [SerializeField] private List<IceCreamBall> listBall = new List<IceCreamBall>();
    [SerializeField] private bool ballIsSwinging;
    [SerializeField] private IceCreamBall ballToLand;
    [SerializeField] private Vector3 stageDimensions;

    [Header("UI")]
    [SerializeField] private GameObject victoryPanel;
    [SerializeField] private TextMeshProUGUI countdownText;

    [Header("SwingingBall")] 
    [SerializeField] private GameObject ballPrefab;
    [SerializeField] private Transform ballFolder;
    [HideInInspector] public Vector2 dir;
    [SerializeField] private float speedBall;

    [Header("RespawnBall")] 
    private bool onTimerRespawn;
    [SerializeField] private float maxTimerRespawnBall;
    [SerializeField] private float timerRespawnBall;

    [Header("Highest Ball")] 
    public GameObject highestBall;
    [SerializeField] private GameObject lineHighestBall;

    [Header("Level Management")] 
    public List<Level> levelList = new List<Level>();
    public int indexLevel;
    [Header("Objective")] 
    [SerializeField] private GameObject linePrefab;
    [SerializeField] private GameObject lineLevel;
    [SerializeField] private float timerVictory;
    [SerializeField] private float maxTimerVictory;

    [Header("Resizable")]
    [SerializeField] private List<GameObject> listObjectToResize = new List<GameObject>();

    [Header("Defeat Mechanics")] 
    [SerializeField] private TextMeshProUGUI txtRemainingBalls;
    public int remainingBalls;
    //TODO Faire monter la camera en fonction de la boule la plus haute
    //Definir un offset pour cette interaction
    [Header("Camera")]
    [SerializeField] private float heighCam; 
    private Camera camera;

    [Header("Borders")] 
    [SerializeField] private Transform borderLeft;
    [SerializeField] private Transform borderRight;

    //Singleton
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        } else if (Instance != null)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        screenRatio = (float) Screen.width/(float) Screen.height;
        SetupScaleResolution();
        camera = Camera.main;
        timerRespawnBall = maxTimerRespawnBall;
        SetupTimerVictory();
        
        indexLevel = 0;
        remainingBalls = ActualLevel().ballToLose;
        txtRemainingBalls.text = remainingBalls.ToString();
        UpdateStageDimension();
        InstanciateNewBall();
        ballIsSwinging = true;
        InstanciateObjectiveToAchieveLevel();
        
    }

    void SetupScaleResolution()
    {
        foreach (var item in listObjectToResize)
        {
            item.transform.localScale = item.transform.localScale * Mathf.Clamp(screenRatio, 0,1);
        }
    }

    // Update is called once per frame
    void Update()
    {
        MoveBorders();
        
        if (Input.GetKeyDown(KeyCode.K))
        {
            NextLevel();
        }
        UpdateStageDimension();
        if (ballIsSwinging && ballToLand)
        {
            /*float minX = camera.ViewportToWorldPoint(new Vector3(0.52f - ActualLevel().gapBarrier, 0.95f, 0)).x + ballToLand.GetComponent<CapsuleCollider2D>().size.x;
            float maxX = camera.ViewportToWorldPoint(new Vector3(0.48f + ActualLevel().gapBarrier, 0.95f, 0)).x - ballToLand.GetComponent<CapsuleCollider2D>().size.x;
            
            
            if (ballToLand.gameObject.transform.position.x <= minX)
            {
                dir = Vector2.right;
            }
            else if (ballToLand.gameObject.transform.position.x >= maxX)
            {
                dir = Vector2.left;
            }*/

            ballToLand.gameObject.transform.Translate(dir*speedBall*Time.deltaTime);
            
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (ballToLand)
                {
                    ballToLand.BallIsFalling(true);
                    listBall.Add(ballToLand);
                    //ballToLand = null;
                    onTimerRespawn = true;
                    ballIsSwinging = false;
                    //InstanciateNewBall();
                }
            }

            if (Input.touchCount > 0)
            {   
                Touch touch = Input.GetTouch(0);
                Debug.Log("It worked");
                if (touch.phase == TouchPhase.Began)
                {
                    if (ballToLand)
                    {
                        ballToLand.BallIsFalling(true);
                        listBall.Add(ballToLand);
                        //ballToLand = null;
                        onTimerRespawn = true;
                        ballIsSwinging = false;
                        //InstanciateNewBall();
                    }
                }
            }
        }
        
        if (onTimerRespawn)
        {
            timerRespawnBall -= Time.deltaTime;
            if (timerRespawnBall <= 0)
            {
                InstanciateNewBall();
                timerRespawnBall = maxTimerRespawnBall;
                onTimerRespawn = false;
                ballIsSwinging = true;
            }
        }
        
        //Get highestBall
        if (listBall.Count > 0)
        { 
            highestBall = HighestBall();
        }
        else
        {
            lineHighestBall.SetActive(false);
        }
        
        if (highestBall)
        {
            float heighBall = highestBall.transform.position.y + (highestBall.GetComponent<CapsuleCollider2D>().size.y/2 * highestBall.transform.localScale.x);
            if (!lineHighestBall.activeSelf)
            {
                lineHighestBall.SetActive(true);
            }
            
            lineHighestBall.transform.position = new Vector3(highestBall.transform.position.x - 0.5f, heighBall,0f);
            lineHighestBall.GetComponentInChildren<TextMeshProUGUI>().text = GetHeightString(heighBall);

            if (heighBall >= lineLevel.transform.position.y)
            {
                countdownText.gameObject.SetActive(true);
                timerVictory -= Time.deltaTime;
                countdownText.text = timerVictory.ToString("F0");
                if (timerVictory <= 0)
                {
                    LevelComplete();    
                }
            }else
            {
                //If the player can't keep the ice cream for more than 3 seconds, reset 
                SetupTimerVictory();
            }
        }
    }

    string GetHeightString(float y)
    {
        float distance = Mathf.Abs(y - GetDownPosScene().y);
        if (distance >= 10f)
        {
            return (distance/10).ToString("F2") + " M";
        }
        else
        {
            return (distance*10).ToString("F2") + " CM";
        }
    }

    void UpdateStageDimension()
    {
        stageDimensions = camera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height,1));
        heighCam = stageDimensions.y;
    }

    void InstanciateNewBall()
    {
        //Instantiate the new ball with a random preset form the level list
        GameObject newBall = Instantiate(ballPrefab, ballFolder);
        newBall.GetComponent<IceCreamBall>().SetupBall(ActualLevel().GetRandomIceCream());
        newBall.transform.localScale = newBall.transform.localScale * Mathf.Clamp(screenRatio, 0,1);

        float xPos = Random.Range(0f, 2f) <1f ? .6f - GetGapBarrier() : .4f +  GetGapBarrier();    //Random value between .4f and .6f (left or right)

        //Instanciate the ball at the top middle of the screen
        Vector3 centerPos = camera.ViewportToWorldPoint(new Vector3(xPos, 0.95f, 0));
        newBall.transform.position = new Vector2(centerPos.x,centerPos.y);
        
        //Check newBall x position to set it's direction at start
        if (newBall.transform.position.x < 0f)
        {
            dir = Vector2.right;
        }
        else if (newBall.transform.position.x > 0f)
        {
            dir = Vector2.left;
        }
    
        ballToLand = newBall.GetComponent<IceCreamBall>();
    }

    float GetGapBarrier()
    {
        
        return levelList[indexLevel].gapBarrier;
    }

    void InstanciateNewBallLevel()
    {
        RemoveBall(ballToLand);
        
        ballIsSwinging = false;
        dir = Vector2.right;
        InstanciateNewBall();
        ballIsSwinging = true;
    }

    void InstanciateObjectiveToAchieveLevel()
    {
        Vector3 downPos = camera.ViewportToWorldPoint(new Vector3(0.43f +  GetGapBarrier(), 0f, 0));
        
        if (lineLevel)
        {
            Destroy(lineLevel);
        }
        
        lineLevel = Instantiate(linePrefab, new Vector3(downPos.x, downPos.y + ActualLevel().heightToWin, 0f),quaternion.identity);
        lineLevel.transform.localScale = lineLevel.transform.localScale * Mathf.Clamp(screenRatio, 0, 1);
        lineLevel.GetComponentInChildren<TextMeshProUGUI>().text = GetHeightString(lineLevel.transform.position.y);
    }

    Vector3 GetDownPosScene()
    {
        return camera.ViewportToWorldPoint(new Vector3(0.62f, 0f, 0));
    }

    //Get the highest ball 
    GameObject HighestBall()
    {
        GameObject highestBall = null;


        foreach (var ball in listBall)
        {
            if (ball.isDown)
            {
                if (highestBall != null)
                {
                    if (ball.transform.position.y + ball.GetComponent<CapsuleCollider2D>().size.y * ball.transform.localScale.y > highestBall.transform.position.y + ball.GetComponent<CapsuleCollider2D>().size.y * ball.transform.localScale.y)
                    {
                        highestBall = ball.gameObject;
                    }
                }
                else
                {
                    highestBall = ball.gameObject;
                }
            }
        }
        return highestBall;
    }

    public void RefreshUI()
    {
        txtRemainingBalls.text = remainingBalls.ToString();
    }

    public Level ActualLevel()
    {
        return levelList[indexLevel];
    }

    void LevelComplete()
    {
        Time.timeScale = 0;
        if (!victoryPanel.activeSelf)
        {
            victoryPanel.SetActive(true);
        }
    }

    void MoveBorders()
    {
        float xLeft = camera.ViewportToWorldPoint(new Vector3(0.5f - GetGapBarrier(), 0f, 0)).x;
        float xRight = camera.ViewportToWorldPoint(new Vector3(0.5f + GetGapBarrier(), 0f, 0)).x;

        borderLeft.position = new Vector3(xLeft - 0.25f ,borderLeft.transform.position.y,borderLeft.transform.position.z);
        borderRight.position = new Vector3(xRight + 0.25f,borderRight.transform.position.y,borderRight.transform.position.z);
    }

    public void NextLevel()
    {
        if (victoryPanel.activeSelf)
        {
            victoryPanel.SetActive(false);
            Time.timeScale = 1f;
        }
        
        indexLevel++;
        CleanLevel();
        remainingBalls = ActualLevel().ballToLose;
        SetupTimerVictory();
        UpdateStageDimension();
        InstanciateNewBallLevel();
        InstanciateObjectiveToAchieveLevel();
        RefreshUI();
        
        
        //TODO faire la gestion du prochain niveau, refresh l'ui
    }

    void SetupTimerVictory()
    {
        timerVictory = maxTimerVictory;
        countdownText.gameObject.SetActive(false);
    }

    void CleanLevel()
    {
        //TODO supprimer les boules actuelles
        foreach (var ball in listBall.ToList())
        {
            RemoveBall(ball);
        }
    }

    public void RemoveBall(IceCreamBall ball)
    {
        listBall.Remove(ball);
        listBall = listBall.Where(s => s != null).Distinct().ToList();
        Destroy(ball);
        Destroy(ball.gameObject);
    }

    public void BallHasFallen()
    {
        remainingBalls--;
        RefreshUI();
        if (remainingBalls == 0)
        {
            Debug.Log("GameOver");
        }
    }
}
