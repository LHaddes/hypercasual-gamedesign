﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMelt : MonoBehaviour
{
    public float timerBeforeStartMelting;

    public float scaleFactor, scaleReductor;
    public float colliderFactor, colliderReductor;

    public float timerColliderActualisation;
    public float saveTimer;

    public bool meltOn;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (meltOn)
        {
            timerBeforeStartMelting -= Time.deltaTime;
            
            
            if (timerBeforeStartMelting <= 0)
            {
                timerColliderActualisation -= Time.deltaTime;
                if (timerColliderActualisation <= 0f)
                {
                    //GetComponent<IceCreamBall>().isDown = false;
                    CapsuleCollider2D actualCollider = GetComponent<CapsuleCollider2D>();
                    CapsuleCollider2D newCollider = gameObject.AddComponent<CapsuleCollider2D>();

                    newCollider.size = new Vector2(actualCollider.size.x, actualCollider.size.y * colliderFactor);
                    newCollider.direction = CapsuleDirection2D.Horizontal;
                
                    transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y * scaleFactor, transform.localScale.z);
                
                    Destroy(actualCollider);

                    colliderFactor -= colliderReductor;
                    scaleFactor -= scaleReductor;

                    timerColliderActualisation = saveTimer;
                }
            

                if (transform.localScale.x <= 0.1f || transform.localScale.y <= 0.1f)
                {
                    GameplayManager.Instance.RemoveBall(GetComponent<IceCreamBall>());
                }
            }
        }
        
        
        
    }
}
