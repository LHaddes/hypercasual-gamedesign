﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallJoint : MonoBehaviour
{
    public SpringJoint2D _springJoint2D;

    public Rigidbody2D rbOther;
    // Start is called before the first frame update
    void Start()
    {
        //_springJoint2D.GetComponent<SpringJoint2D>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<Rigidbody2D>() == true)
        {
            rbOther = other.gameObject.GetComponent<Rigidbody2D>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (rbOther != null)
        {
            _springJoint2D.connectedBody = rbOther;
        }
    }
}