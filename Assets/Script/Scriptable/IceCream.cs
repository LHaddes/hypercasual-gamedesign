﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New IceCream", menuName = "Ice Cream")]
public class IceCream : ScriptableObject
{
    
    [Header("Melting")]
    [Tooltip("Le temps avant que la boule ne commence à fondre.")]
    public float timeBeforeMelt;
    [Tooltip("Le facteur par lequel on réduit constamment la scale (scaleFactor -= scaleReductor).")]
    public float scaleReductor;
    [Tooltip("Le facteur par lequel on réduit constamment le collider (colliderFactor -= colliderReductor).")]
    public float colliderReductor;
    [Tooltip("Le temps entre chaque réduction du collider et de la scale de la boule de glace.")]
    public float timerColliderActualisation;

    [Header("Parameters")]
    public Color spriteColor;
    public ScaleBall scaleBall;
    public float speedFall;
    //TODO Mettre les informations nécessaires aux différentes boules de glace
}


public enum ScaleBall
{
    Little,
    Medium,
    Big,
}