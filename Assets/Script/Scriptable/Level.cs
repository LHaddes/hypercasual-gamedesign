﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Card", menuName = "Level")]
public class Level : ScriptableObject
{
    public float heightToWin;
    public int ballToLose;
    [Range(0.1f, 0.5f)]
    public float gapBarrier;

    public List<IceCream> listOfBalls;


    public IceCream GetRandomIceCream()
    {
        return listOfBalls[Random.Range(0, listOfBalls.Count)];
    } 
}
