﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GoalToAchieve : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI heightText;
    // Start is called before the first frame update
    void Start()
    {
        heightText.text = (GameplayManager.Instance.ActualLevel().heightToWin*10) + " cm";
    }
}
