﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoCurrentHeigh : MonoBehaviour
{
    private GameplayManager _gameplayManager;
    [SerializeField] private GameObject childText;
    // Start is called before the first frame update
    void Start()
    {
        _gameplayManager = GameplayManager.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        if (_gameplayManager.highestBall)
        {
            Hide(false);
        }
        else
        {
            Hide(true);
        }
        
    }

    void Hide(bool value)
    {
        childText.SetActive(value);
    }
}
