﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class towerGrab : MonoBehaviour
{
    private const float G = 0.6674f;
    public Rigidbody2D rb;

    public static List<towerGrab> tower;

    private void FixedUpdate()
    {
        foreach (towerGrab towerGrab in tower)
        {
            if (towerGrab != this)
            {
                Attract(towerGrab); 
            }
            
        }
    }

    private void OnEnable()
    {
        if (tower == null)
        {
            tower = new List<towerGrab>();
        }
        tower.Add(this);
    }

    private void OnDisable()
    {
        tower.Remove(this);
    }

    void Attract(towerGrab objToAttract)
    {
        Rigidbody2D rbToAttract = objToAttract.rb;

        Vector2 direction = rb.position - rbToAttract.position;
        float distance = direction.magnitude;

        float forceMagnitude = G * (rb.mass * rbToAttract.mass) / Mathf.Pow(distance, 2);
        Vector2 force = direction.normalized * forceMagnitude;
        
        rbToAttract.AddForce(force);
    }
}
